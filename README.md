### o7mod

The **o7mod** is an ever evolving experiment to extend *Minecraft* JE client.\
The main goal of the project is to provide a set of **fair to use utilities** for more *advanced play styles*. 