package io._7Gv.o7mod;

import io._7Gv.o7mod.client.input.Keybinds;
import io._7Gv.o7mod.client.module.Module;
import io._7Gv.o7mod.client.module.ModuleDisplayable;
import io._7Gv.o7mod.client.module.ModuleManager;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.StringRenderable;
import net.minecraft.text.Style;

import java.util.LinkedList;

public final class O7mod implements ModInitializer
{

        private static O7mod instance;
        private ModuleManager modman;

        @Override
        public void onInitialize()
        {
                instance = this;

                modman = new ModuleManager();
                modman.init();

                HudRenderCallback.EVENT.register(this::onHudRender);
        }

        public static O7mod getInstance()
        {
                return instance;
        }

        private void onHudRender(MatrixStack mat, float b)
        {
                modman.display(mat);
        }
}
