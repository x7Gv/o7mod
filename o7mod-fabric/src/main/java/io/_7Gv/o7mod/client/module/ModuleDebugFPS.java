package io._7Gv.o7mod.client.module;

import io._7Gv.o7mod.O7mod;
import io._7Gv.o7mod.client.input.Keybinds;
import net.fabricmc.fabric.api.event.client.ClientTickCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.math.MatrixStack;
import org.lwjgl.glfw.GLFW;

public class ModuleDebugFPS extends ModuleDisplayable
{
        private final KeyBinding key;

        public ModuleDebugFPS()
        {
                super(true, ModuleType.DEBUG);

                key = Keybinds.bindingFactory("debug_fps", "debug", GLFW.GLFW_KEY_Z);
                this.keymap.put(this, key);
        }

        @Override
        void init()
        {
                ClientTickCallback.EVENT.register((minecraftClient -> {
                        while (key.wasPressed()) {
                                run();
                        }
                }));
        }

        @Override
        void run()
        {
                this.enabled = !enabled;
        }

        @Override
        public void display(MatrixStack mat, int x0, int y0)
        {
                TextRenderer renderer = MinecraftClient.getInstance().textRenderer;
                String text = mc.fpsDebugString;

                int w = renderer.getWidth(text);
                int h = renderer.fontHeight;

                DrawableHelper.fill(mat, x0, y0, w + x0 + 4, h + y0 + 4, -1873784752);
                renderer.drawWithShadow(mat, text, x0 + 3, y0 + 3, 0xffffff);
        }
}
