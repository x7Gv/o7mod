package io._7Gv.o7mod.client.module;

import io._7Gv.o7mod.client.input.Keybinds;
import net.fabricmc.fabric.api.event.client.ClientTickCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.math.MatrixStack;
import org.lwjgl.glfw.GLFW;

import java.text.DecimalFormat;

public class ModuleDebugPos extends ModuleDisplayable
{
        private final KeyBinding key;
        private final DecimalFormat df;

        public ModuleDebugPos()
        {
                super(true, ModuleType.DEBUG);

                this.df = new DecimalFormat("#.###");

                key = Keybinds.bindingFactory("debug_pos", "debug", GLFW.GLFW_KEY_P);
                this.keymap.put(this, key);
        }

        @Override
        void init()
        {
                ClientTickCallback.EVENT.register((minecraftClient -> {

                        while (key.wasPressed()) {
                                run();
                        }
                }));
        }

        @Override
        void run()
        {
                this.enabled = !enabled;
        }

        @Override
        public void display(MatrixStack mat, int x0, int y0)
        {
                TextRenderer renderer = MinecraftClient.getInstance().textRenderer;

                assert mc.player != null;
                String text = String.format("(%.1f, %.1f, %.1f) - %s", mc.player.getPos().x, mc.player.getPos().y, mc.player.getPos().z, (mc.player.getHorizontalFacing().asString().toUpperCase().charAt(0)));

                int w = renderer.getWidth(text);
                int h = renderer.fontHeight;

                DrawableHelper.fill(mat, x0, y0, w + x0 + 4, h + y0 + 4, -1873784752);
                renderer.drawWithShadow(mat, text, x0 + 3, y0 + 3, 0xffffff);
        }
}
