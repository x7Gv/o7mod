package io._7Gv.o7mod.client.module;

import io._7Gv.o7mod.client.input.Keybinds;
import net.fabricmc.fabric.api.event.client.ClientTickCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.math.MatrixStack;
import org.lwjgl.glfw.GLFW;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ModuleDebugTime extends ModuleDisplayable
{
        private final KeyBinding key;

        public ModuleDebugTime()
        {
                super(true, ModuleType.DEBUG);

                key = Keybinds.bindingFactory("debug_time", "debug", GLFW.GLFW_KEY_K);
                this.keymap.put(this, key);
        }

        private LocalTime timeOfDay()
        {
                assert mc.world != null;
                long ticks = ((mc.world.getTimeOfDay() + 12000 - 3000L) % 24000L);
                long seconds = ticks / 20L;

                return LocalTime.ofSecondOfDay(seconds);
        }

        @Override
        public void display(MatrixStack mat, int x0, int y0)
        {
                TextRenderer renderer = MinecraftClient.getInstance().textRenderer;
                String text = "t: " + timeOfDay().format(DateTimeFormatter.ISO_LOCAL_TIME);

                int w = renderer.getWidth(text);
                int h = renderer.fontHeight;

                DrawableHelper.fill(mat, x0, y0, w + x0 + 4, h + y0 + 4, -1873784752);
                renderer.drawWithShadow(mat, text, x0 + 3, y0 + 3, 0xffffff);
        }

        @Override
        void init()
        {
                ClientTickCallback.EVENT.register((minecraftClient -> {
                        while (key.wasPressed()) {
                                run();
                        }
                }));
        }

        @Override
        void run()
        {
                this.enabled = !enabled;
        }
}
