package io._7Gv.o7mod.client.module;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;

import java.util.HashMap;

public abstract class Module
{
        protected HashMap<Module, KeyBinding> keymap;

        protected ModuleType type;
        protected MinecraftClient mc;

        protected boolean enabled;

        public Module()
        {
                this.mc = MinecraftClient.getInstance();

                this.keymap = new HashMap<>();
                this.enabled = true;
                this.type = ModuleType.UNDEFINED;
        }

        public Module(boolean enabled, ModuleType type)
        {
                this.mc = MinecraftClient.getInstance();

                this.keymap = new HashMap<>();
                this.enabled = enabled;
                this.type = type;
        }

        abstract void init();

        abstract void run();

        public HashMap<Module, KeyBinding> getKeymap()
        {
                return keymap;
        }

        public ModuleType getType()
        {
                return type;
        }

        public boolean isEnabled()
        {
                return enabled;
        }
}
