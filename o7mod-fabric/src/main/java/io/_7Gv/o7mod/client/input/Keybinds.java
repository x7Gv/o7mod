package io._7Gv.o7mod.client.input;

import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import org.lwjgl.glfw.GLFW;

import java.util.HashSet;

public class Keybinds
{
        public static HashSet<KeyBinding> binded = new HashSet<>();

        public static KeyBinding bindingFactory(String name, String cat, int key)
        {
                KeyBinding keyBinding;
                keyBinding = new KeyBinding(
                        "key.o7mod." + name,
                        InputUtil.Type.KEYSYM, key,
                        "category.o7mod." + cat
                );

                KeyBindingHelper.registerKeyBinding(keyBinding);

                binded.add(keyBinding);
                return keyBinding;
        }
}
