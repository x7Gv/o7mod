package io._7Gv.o7mod.client.module;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.math.MatrixStack;

import java.util.*;

public class ModuleManager
{
        private LinkedList<Module> modules;
        private MinecraftClient mc;

        public void init()
        {
                this.mc = MinecraftClient.getInstance();

                modules = new LinkedList<>();

                addModule(new ModuleDebugFPS());
                addModule(new ModuleDebugTime());
                addModule(new ModuleDebugPos());
                addModule(new ModuleDebugWorldData());

                modules.forEach(Module::init);
        }

        public void display(MatrixStack mat)
        {
                if (mc.options.debugEnabled || mc.options.hudHidden) {
                        return;
                }

                LinkedList<Module> enabledModules = getEnabledModules();

                for (int i = 0; i < enabledModules.size(); i++) {

                        Module module = enabledModules.get(i);
                        if (module instanceof ModuleDisplayable) {
                                ModuleDisplayable moddis = (ModuleDisplayable) module;
                                moddis.display(mat, 10, 10 + (i * 15));
                        }
                }
        }

        private void addModule(Module module)
        {
                modules.add(module);
        }

        public LinkedList<Module> getModules()
        {
                return modules;
        }

        public LinkedList<Module> getEnabledModules()
        {
                LinkedList<Module> enabledL = new LinkedList<>();

                for (Module module : modules) {

                        if (module.enabled) {
                                enabledL.add(module);
                        }
                }

                return enabledL;
        }
}
