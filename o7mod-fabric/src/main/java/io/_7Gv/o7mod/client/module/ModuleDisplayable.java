package io._7Gv.o7mod.client.module;

import net.minecraft.client.util.math.MatrixStack;

public abstract class ModuleDisplayable extends Module
{
        public ModuleDisplayable(boolean b, ModuleType debug)
        {
        }

        public abstract void display(MatrixStack mat, int x0, int y0);
}
