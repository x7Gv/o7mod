package io._7Gv.o7mod.mixin;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.registry.Registry;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Mixin(ItemStack.class)
public abstract class O7MixinItemTooltip
{
        @Final
        @Shadow
        private Item item;

        @ModifyVariable(
                method = "getTooltip",
                ordinal = 0,
                at = @At(value = "TAIL", target = "Lnet/minecraft/item/ItemStack;getTooltip(Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/client/item/TooltipContext;)Ljava/util/List;"))
        private List<Text> o7_onGetTooltip(List<Text> list)
        {
                if (item instanceof MiningToolItem) {
                        MiningToolItem miningToolItem = (MiningToolItem) item;
                        float spd = miningToolItem.miningSpeed;
                        list.add(new LiteralText("spd: " + spd).formatted(Formatting.GREEN));
                }

                return list;
        }
}
